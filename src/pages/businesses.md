---
layout: ../layouts/PageLayout.astro
title: "businesses"
---

This page contains some basic information on businesses I've started (or at least attempted!)



<ul class="grid effect-2" id="grid">

  <li>
    <a href="https://teamlab.wtf">
      <img src="/assets/img/posts/contract-highlighter/contract_post.png"/>
    </a>
    This is a contract highlighter for google workplace apps.
  </li>

  <li>
    <a href="https://teamlab.wtf">
      <img src="/assets/img/projects/teamlab.png"/>
    </a> 
    Teamlab is a team generator for any team sport. Currently needs some marketing!
  </li>


  <li>
    <a href="https://davidawad.gumroad.com/l/pleading-template">
      <img src="/assets/img/projects/pleading.png"/>
    </a>
    I developed one of very few pleading paper templates for google docs. It came about as it was far too painful to get anything done sending google docs back and forth.
  </li>


  <li>
    <a href="https://davidawad.gumroad.com/l/ropsten">
      <img src="/assets/img/projects/ropsten.png"/>
    </a>
    ropsten.money (defunct). The idea here was that I mined ETH on testnet in order to sell it online to teachers, large developer teams and other people who wanted to use the testnet for educational or professional purposes.
  </li>


 
</ul>
